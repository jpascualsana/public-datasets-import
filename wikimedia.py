import sys, argparse, logging, json
from wikimedia.wikimedia import Wikimedia
from utils.logger_utils import setLoging

def main(argv):
    parser = argparse.ArgumentParser(
        description='Example:\nwikimedia.py https://wikipedia.org/api.php -a -v',
        formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('wikimediaUrl', metavar='wikimediaUrl', type=str, nargs='+',
                        help='Wikimedia Url to parse')
    parser.add_argument("-a", "--all", help="Get all pages", action='store_true')
    parser.add_argument("-s", "--search", help="Search pages that title contains ", type=str, nargs="+")
    parser.add_argument('--verbose', '-v', action='count',
                        help="Set logging to INFO with -v. Set logging to DEBUG with -vv ")

    args = parser.parse_args()

    setLoging(args.verbose)

    for url in args.wikimediaUrl:
        wm = Wikimedia(url=url)
        if args.search:
            for s in args.search:
                logging.info(wm.search(s, results="max"))

        if args.all:
            logging.info("Trying to retrieve " + str(wm.getTotalPages()) + " pages")
            for page in wm.getAllPages():
                logging.info(wm.page(page["title"]))

if __name__ == '__main__':
    main(sys.argv[1:])

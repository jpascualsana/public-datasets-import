import requests, logging, json
from utils.logger_utils import log
from bs4 import BeautifulSoup
from urllib.parse import urlparse

OR = 'http://www.ondarossa.info/'
SITEMAP = 'sitemap.xml'

class OndaRossa(object):

    """
    Get all the urls publicated on OndaRossa
    :type(str) If a type is specified return only the type url specified. For example type="newstrasmissioni" will
    return urls like: http://www.ondarossa.info/newstrasmissioni/2019/01/piano-solo
    """
    def getAllUrl(self, type=None):
        result = requests.get(OR+SITEMAP)
        sitemap = BeautifulSoup(result.content, features="lxml")
        for loc in sitemap.find_all('loc'):
            logging.info("SITEMAP url: " + loc.contents[0])
            locResult = requests.get(loc.contents[0])
            locmap = BeautifulSoup(locResult.content, features="lxml")
            for pageurl in locmap.find_all("loc"):
                logging.info("PAGE url: " + pageurl.contents[0] + ' TYPE:' + str(type))
                purl = urlparse(pageurl.contents[0])
                if type is None:
                    yield pageurl.contents[0]
                elif type == purl.path.split('/')[1]:
                    yield pageurl.contents[0]

    def getPage(self, url):
        return OndaRossaPage(url)

class OndaRossaPage(object):

    def __init__(self, url):
        self.url = url
        self._metadata = None
        self._htmlPage = None
        self._podcast = None

    @property
    def metadata(self):
        if self._metadata is None:
            logging.debug("Fetching metadata for " + self.url)
            self._metadata = []
            for m in self._getLdJson():
                self._metadata.append(m)
        return self._metadata

    @property
    def htmlPage(self):
        if self._htmlPage is None:
            logging.debug("Fetching html page for " + self.url)
            self._htmlPage = requests.get(self.url).content
        return self._htmlPage

    @property
    def siteMap(self):
        return BeautifulSoup(self.htmlPage, "html.parser")

    @property
    def podcast(self):
        podcast = []
        if self._podcast is None:
            logging.debug("Fetching podcasts for " + self.url)
            for audioTag in self._getAudio():
                podcast.append(Podcast(audioTag))
            self._podcast = podcast
        return self._podcast

    """
    Get <script type="application/ld+json"> from an html page in json format
    """
    def _getLdJson(self):
        for loc in self.siteMap.find_all('script', {"type": "application/ld+json"}):
            yield json.loads(loc.contents[0])

    """
    Get all audio tags inside the siteMap
    :return empty [] or [] with <audio> tags
    """
    @log
    def _getAudio(self):
        aud = []
        art = self.siteMap.find('article')
        if art is not None:
            aud = art.find_all('audio')
        return aud


class Podcast(object):
    def __init__(self, audiotag):
        logging.info("Creating podcast object: " + audiotag.get("title"))

        self.audiotag = audiotag
        self.source = audiotag.find_all("source")
        self.title = audiotag.get("title")

    '''
    Get the <source> tags of specific type, for example 'audio/ogg'
    '''
    def getSourceByType(self, type):
        return self.audiotag.find_all("source", {"type": type})

    '''
    Get the link src of a tag <source>
    '''
    def getLinkFromSource(self, source):
        return source.get("src")

    '''
    Get the type of a tag <source>
    '''
    def getTypeFromSource(self, source):
        return source.get("type")

    def json(self):
        return {'title': self.title, 'source': self._sourcesToJson()}

    '''
    Create a json from source tags
    '''
    def _sourcesToJson(self):
        list = []
        for source in self.source:
            list.append({'src': self.getLinkFromSource(source), 'type': self.getTypeFromSource(source)})
        return list

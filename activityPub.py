import sys, argparse, logging, json
from activityPub.activitypub import ActivityPub
from utils.logger_utils import setLoging

def main(argv):
    parser = argparse.ArgumentParser(
        description='Example:'
                    '\n\t* Get 10 posts of ActivityPub address:'
                    '\n\t  run.py user@server.dom -a 10 -v'
                    '\n\t* Get first 10 activity for given addresses and feeds url:'
                    '\n\t  run.py user@server.dom user@server2.dom -f https://server.dom/users/user/outbox -a 10'
                    '\n\t* Get user info on json format '
                    '\n\t  run.py user@server.dom -u '
                    '\n\t* Get updates from specific post'
                    '\n\t  run.py user@server.dom --updates https://server.dom/users/user/statuses/postId ',
                    formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('address', metavar='address', type=str, nargs='*',
                        help='Ex: user@server.dom. One ore more ActivityPub address where to retrieve the info')
    parser.add_argument("-u", "--user", help="Print user info in JSON format and exit. Only with address, not for feed urls.", action='store_true')
    parser.add_argument("-f", "--feed", help="Parse feed url.", type=str, nargs='+')
    parser.add_argument("-a", "--activity", help="Show last activity for given address or feed urls. By default show all activity",
                        type=int)
    parser.add_argument("--first", help="Start parsing for first activity instead of last when parsing posts", action='store_true')
    parser.add_argument("--updates", help="Get updates from specific post id", type=str)
    parser.add_argument('--verbose', '-v', action='count', help="Set logging to INFO with -v. Set logging to DEBUG with -vv ")

    args = parser.parse_args()

    if argv.__len__() == 0:
        parser.print_help(sys.stderr)
        sys.exit(0)

    setLoging(args.verbose)

    logging.info("ActivityPub activity parser:")

    # TODO: check more beautiful way to do that on argparse docs
    limit = args.activity or None

    if args.address is not None:
        for person in args.address:
            logging.info("Getting info for " + person)
            ac = ActivityPub(address=person)
            if args.user:
                print(json.dumps(ac.actor.user_json))
                sys.exit(0)
            if args.updates:
                print(json.dumps(ac.get_updates(args.updates)))
                sys.exit(0)
            ac.get_feed_items(limit=limit, first=args.first)

    if args.feed is not None:
        for feed in args.feed:
            logging.info("Getting info for feed url " + feed)
            ac = ActivityPub()
            if args.updates:
                print(json.dumps(ac.get_updates(args.updates, url=feed)))
                sys.exit(0)
            ac.get_feed_items(url=feed, limit=limit, first=args.first)

if __name__ == '__main__':
    main(sys.argv[1:])

import sys, argparse, logging, json
from rss.rss import Rss
from utils.logger_utils import setLoging

def printPost(post):
    print("-----------------")
    print("\n" + post.title)
    print(post.published)
    print("\n" + post.summary)
    print("\nLink: " + post.link)


def main(argv):
    parser = argparse.ArgumentParser(
        description='Example:\n\trun.py https://url -s',
        formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('feedUrl', metavar='feedUrl', type=str, nargs='+',
                        help='Feed url to parse')
    parser.add_argument("-s", "--show", help="Show parsed posts", action='store_true')
    parser.add_argument("--updates", help="Get updates from specific new guid", type=str)
    parser.add_argument('--verbose', '-v', action='count',
                        help="Set logging to INFO with -v. Set logging to DEBUG with -vv ")

    args = parser.parse_args()

    setLoging(args.verbose)

    for url in args.feedUrl:
        rss = Rss(url)
        if args.updates:
            for update in rss.get_updates(args.updates):
                printPost(update)
        if args.show:
            print("Total posts: ", len(rss.news.entries))
            for post in rss.news.entries:
                printPost(post)

if __name__ == '__main__':
    main(sys.argv[1:])

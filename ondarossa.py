import sys, argparse, logging, json
from ondarossa.ondarossa import OndaRossa
from utils.logger_utils import setLoging

def printMetadata(page):
    logging.info("Printing metadata for " + page.url)
    for metadata in page.metadata:
        print(json.dumps(metadata))

def printPodcastData(page):
    logging.info("Printing podcasts for " + page.url)
    for p in page.podcast:
        print(json.dumps(p.json()))

def main(argv):

    parser = argparse.ArgumentParser(
        description='Example:'
                    '\n\t* Get all pages and print metadata:'
                    '\n\t  python ondarossa.py -a -m'
                    '\n\t* Get all pages from newsredazione:'
                    '\n\t  python ondarossa.py -a -m -t newsredazione -v'
                    '\n\t* Get specific page from url and print metadata'
                    '\n\t  python ondarossa.py -m -u "http://www.ondarossa.info/newsredazione/2010/04/tentata-evasione-e-botte-nel-cie-torino -v"'
                    '\n\t* Print podcast data for all pages:'
                    '\n\t  python ondarossa.py -a -p',
        formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("-a", "--all", help="Get all Ondarossa articles", action='store_true')
    parser.add_argument("-t", "--type", help="Specify the type of articles to get, for example 'newsredazione'",
                        type=str, default=None, nargs=1)
    # parser.add_argument("-d", "--download", help="If found, download audio file in ogg format", action='store_true')
    parser.add_argument("-m", "--metadata", help="Print page metadata", action='store_true')
    parser.add_argument("-p", "--podcast", help="Print podcast data", action='store_true')
    parser.add_argument("-u", "--url", help="Import specific url pages", type=str, nargs='+')
    parser.add_argument('--verbose', '-v', action='count', help="Set logging to INFO with -v. Set logging to DEBUG with -vv ")


    args = parser.parse_args()
    setLoging(args.verbose)
    logging.info("Starting program")

    if argv.__len__() == 0:
        parser.print_help(sys.stderr)
        sys.exit(0)

    onda = OndaRossa()
    if args.url:
        for articleurl in args.url:
            op = onda.getPage(articleurl)
            if args.metadata:
                printMetadata(op)
            if args.podcast:
                printPodcastData(op)
            sys.exit(0)

    if args.all:
        i = 0
        onda = OndaRossa()
        if args.type is not None:
            args.type = args.type[0]
        for articleurl in onda.getAllUrl(type=args.type):
            logging.info(str(i) + " Fetching article: " + articleurl)
            op = onda.getPage(articleurl)
            if args.metadata:
                printMetadata(op)
            if args.podcast:
                printPodcastData(op)
            i += 1

    sys.exit(0)

if __name__ == '__main__':
    main(sys.argv[1:])

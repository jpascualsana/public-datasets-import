import json
import tomd
from wordpress import API

# Various endpoints
WP_API = 'wp-json'
WP_POSTS = 'posts/'
WP_PAGES = 'pages/'

"""
This class will store relevant information about wordpress pages and posts
"""
class Summary(object):
    def __init__(self, data):
        self.id = data['id'] or None
        self.title = data['title'] or None
        self.content = data['content']['rendered'] or None
        self.creationDate = data['date'] or None
        self.lastModified = data['modified'] or None
        self.type = data['type'] or None
        self.slugs = data['slug'] or None
        self.link = data['link'] or None
        self.authorId = data['author'] or None
        self._authorLink = data['_links']['author'] or None
        self._raw = data
        self.contentMD = Summary.htmlToMarkdown(self.content)

    @staticmethod
    def htmlToMarkdown(content):
        return tomd.convert(content)


    def _json(self):
        return {'id': self.id,
                'title': self.title,
                # 'content': self.content,
                'creationDate': self.creationDate,
                'lastModified': self.lastModified,
                'type': self.type,
                'slugs': self.slugs,
                'link': self.link,
                'authorId': self.authorId}

class Wrapper(object):

    def __init__(self, url):
        self.url = url
        self.postsSummaried = []
        self.pagesSummaried = []

        self.wpapi = API(
            url=self.url,
            api=WP_API,
            version='wp/v2',
            consumer_key='',
            consumer_secret='',
            no_auth=True,
        )

    def get(self,url):
        print("Get " + url)
        res = self.wpapi.get(url)
        print("Done.")
        return res

    def getPosts(self, posts=''):
        self.posts = self.get(WP_POSTS + str(posts)).json()
        # print(self.posts.__length__)
        return self.posts

    def getPages(self, pages=''):
        self.pages = self.get(WP_PAGES + str(pages)).json()
        return self.pages

    def summaryPosts(self, verbose=False):
        if self.posts:
            if 'id' in self.posts:
                s = Summary(self.posts)
                self.postsSummaried.append(s)
                if verbose:
                    print(s._json())
            else:
                for post in self.posts:
                    s = Summary(post)
                    self.postsSummaried.append(s)
                    if verbose:
                        print(s._json())

    def summaryPages(self, verbose=False):
        if self.pages:
            if 'id' in self.pages:
                s = Summary(self.pages)
                self.pagesSummaried.append(s)
                if verbose:
                    print(s._json())
            else:
                for pages in self.pages:
                    s = Summary(pages)
                    self.pagesSummaried.append(s)
                    if verbose:
                        print(s._json())
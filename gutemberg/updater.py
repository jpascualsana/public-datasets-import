'''
This file will contain necessary functions to update and download gutemberg.org RDF catalog
'''

import urllib.request, os, progressbar, tarfile
from datetime import datetime

'''
Class used to show download progress
'''
class progresObj(object):
    def __init__(self):
        self.pbar = None
    def show_progress(self, count, block_size, total_size):
        if self.pbar is None:
            self.pbar = progressbar.ProgressBar(maxval=total_size)
            self.pbar.start()

        downloaded = count*block_size
        if downloaded < total_size:
            self.pbar.update(downloaded)
        else:
            self.pbar.finish()
            self.pbar = None

'''''
Used to extract catalog file 
'''
def extract(bzfile='data.cache/rdf-files.tar.bz2', dest='data.cache/'):
    print("Extracting tarball", bzfile, "to", dest)
    tar = tarfile.open(bzfile, "r:bz2")
    tar.extractall(dest)
    tar.close()
    print("Extracion finished")

def download(catalog_url, download_path="data.cache/rdf-files.tar.bz2"):
    print("Downloading catalog from", catalog_url)
    po = progresObj()
    # Download the file
    urllib.request.urlretrieve(catalog_url, download_path, po.show_progress)
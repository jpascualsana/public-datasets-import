# Gutemberg project

After read the documentation of [gutemberg project](https://www.gutenberg.org/wiki/Gutenberg:Information_About_Robot_Access_to_our_Pages)
I found that the best way to extract the information of all the books is to 
download [RDF/XML](https://www.gutenberg.org/wiki/Gutenberg:Feeds#The_Complete_Project_Gutenberg_Catalog) 
and parse it locally to extract the information.

To parse it I get inspiration from [this](https://github.com/pravinyo/gutenberg-catalog-parsing)
script, so many thanks. 

## Use

```
Gutemberg.org catalog parser:
usage: run.py [-h] [-p] [--init INIT] [--limit LIMIT] [-s] [-d [DOWNLOAD]]
              [-e [EXTRACT]]

Example:
	* Parse with range of books:
	  run.py -p --init 22 --limit 32
	* Download, extract, parse and store:
	  run.py -d -e -p -s

optional arguments:
  -h, --help            show this help message and exit
  -p, --parse           Parse cached catalog on: 'data.cache/cache/epub/'.
                        Accept --init, --limit, --store|s
  --init INIT           Used with --parse|-p. Set init
  --limit LIMIT         Used with --parse|-p. Set limit
  -s, --store           NOT IMPLEMENTED. Used with --parse|-p. Store results
                        on a database
  -d [DOWNLOAD], --download [DOWNLOAD]
                        Download RDF catalog from
                        https://www.gutenberg.org/cache/epub/feeds/rdf-
                        files.tar.bz2 to default data.cache/rdf-files.tar.bz2
  -e [EXTRACT], --extract [EXTRACT]
                        Extract data.cache/rdf-files.tar.bz2 to data.cache/ or
                        to specified dir
```

When you parse the RDF file it returns a tuple like:

```python
return (Id, Title, Download, Rights, epub_img, epub_no_img, kindle_img, kindle_no_img, zip_html, text, cover)
```

If you specify `--store` option it **SIMULATE** to store the tuple on a database. Implementation of store the 
information is a future _TODO_.

## TODO's

* Create an 'update' function that update the `RDF` file.

* Store books on a database for post usage and implement update functions.

* Function to download ebooks files on different formats
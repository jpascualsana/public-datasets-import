import sys, argparse
from urllib.parse import urlparse

from wordPress.wordPress import Wrapper

def main(argv):
    print("Wordpress blogs parser:")

    parser = argparse.ArgumentParser(
        description='Example:'
                    '\n\trun.py https://noblogs.org --post -summary -v',
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('url', metavar='url', type=str, nargs='+',
                        help='One ore more URL of wordpress blog (compatible with APIv2, wordpress version > 4.7)')
    parser.add_argument("-p", "--posts",
                        help="Get all post or specific by id",
                        type=int, const=True, nargs='?')
    parser.add_argument("-a", "--pages",
                        help="Get all pages or specific by id",
                        type=int, const=True, nargs='?')
    parser.add_argument('--verbose', '-v', action="store_true", help="Verbose summaries or json responses. If summary "
                                                                     "activated disable json response")
    parser.add_argument('--summary', '-s', action="store_true", help="Parse json to get a resume for each response")


    args = parser.parse_args()

    for url in args.url:
        wp = Wrapper(url)
        if args.posts:
            if args.posts is True:
                wp.getPosts()
            elif isinstance(args.posts, int):
                wp.getPosts(posts=args.posts)
            if args.summary:
                wp.summaryPosts(verbose=args.verbose)
            elif args.verbose:
                print(wp.posts)
        if args.pages:
            if args.pages is True:
                wp.getPages()
            elif isinstance(args.pages, int):
                wp.getPages(pages=args.pages)
            if args.summary:
                wp.summaryPages(verbose=args.verbose)
            elif args.verbose:
                print(wp.pages)

if __name__ == '__main__':
    main(sys.argv[1:])

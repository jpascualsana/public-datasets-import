# Wikimedia importer

This script use the mediawiki library installable throught `pip install pymediawiki` and extends the class `MediaWiki`.

It can import all pages from a Wikimedia instance or search by title and then, using the page object from `pymediawiki`
get all information of a page.


## Use

```
usage: wikimedia.py [-h] [-a] [-s SEARCH [SEARCH ...]] [--verbose]
                    wikimediaUrl [wikimediaUrl ...]

Example:
wikimedia.py https://wikipedia.org/api.php -a -v

positional arguments:
  wikimediaUrl          Wikimedia Url to parse

optional arguments:
  -h, --help            show this help message and exit
  -a, --all             Get all pages
  -s SEARCH [SEARCH ...], --search SEARCH [SEARCH ...]
                        Search pages that title contains
  --verbose, -v         Set logging to INFO with -v. Set logging to DEBUG with
                        -vv
```


## TODO's

* Better support on get page content. Add support on get it in Markdown.

* Better support for search from categories or retrieve also categories when it get all pages.

* Support for download images and store it on hard drive.

* Get last changes of the wiki.
 

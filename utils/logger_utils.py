import logging, functools

logging.getLogger(__name__).addHandler(logging.NullHandler())
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'

def log(func):
    logger = logging.getLogger(func.__module__)

    @functools.wraps(func)
    def decorator(self, *args, **kwargs):
        logger.debug('Entering: %s', func.__name__)
        result = func(self, *args, **kwargs)
        logger.debug(result)
        logger.debug('Exiting: %s', func.__name__)
        return result

    return decorator

def setLoging(logLevel):
    if logLevel == 1:
        logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)
    elif logLevel is not None and logLevel > 1:
        logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)

"""This module contains methods to make POST and GET requests.
Inspired by https://github.com/python-telegram-bot/python-telegram-bot/blob/master/telegram/utils/request.py
"""
import logging
import urllib3, json
from urllib3.util.timeout import Timeout

logging.getLogger('urllib3').setLevel(logging.WARNING)

class Request(object):
    def __init__(self,
                 con_pool_size=1,
                 connect_timeout=5.,
                 read_timeout=5.
                 ):

        self._connect_timeout = connect_timeout

        kwargs = dict(
            maxsize=con_pool_size,
            timeout=urllib3.Timeout(
                connect=self._connect_timeout, read=read_timeout, total=None))


        mgr = urllib3.PoolManager(**kwargs)
        self._con_pool = mgr

    @property
    def con_pool_size(self):
        """The size of the connection pool used."""
        return self._con_pool_size

    def stop(self):
        self._con_pool.clear()

    @staticmethod
    def _parse(json_data):

        try:
            decoded_s = json_data.decode('utf-8')
            data = json.loads(decoded_s)
        except UnicodeDecodeError:
            logging.getLogger(__name__).debug(
                'Logging raw invalid UTF-8 response:\n%r', json_data)
            raise UnicodeDecodeError('Server response could not be decoded using UTF-8')
        except ValueError:
            raise ValueError('Invalid server response')

        return data

    def _request_wrapper(self, *args, **kwargs):
        try:
            resp = self._con_pool.request(*args, **kwargs)
        except urllib3.exceptions.TimeoutError:
            raise TimedOut()
        except urllib3.exceptions.HTTPError as error:
            # HTTPError must come last as its the base urllib3 exception class
            # TODO: do something smart here; for now just raise NetworkError
            raise NetworkError('urllib3 HTTPError {0}'.format(error))

        if 200 <= resp.status <= 299:
            # 200-299 range are HTTP success statuses
            return resp.data

        try:
            message = self._parse(resp.data)
        except ValueError:
            message = 'Unknown HTTPError'

        if resp.status in (401, 403):
            raise Unauthorized(message)
        elif resp.status == 400:
            raise BadRequest(message)
        elif resp.status == 404:
            raise NotFound()
        elif resp.status == 502:
            raise NetworkError('Bad Gateway')
        else:
            raise NetworkError('{0} ({1})'.format(message, resp.status))


    def get(self, url, timeout=None, **kwargs):

        urlopen_kwargs = kwargs or {}

        if timeout is not None:
            urlopen_kwargs['timeout'] = Timeout(read=timeout, connect=self._connect_timeout)

        result = self._request_wrapper('GET', url, **urlopen_kwargs)

        return self._parse(result)

import os, os.path, argparse, sys
from gutemberg.gutemberg import parse
from gutemberg import updater

CACHE_PATH = 'gutemberg/data.cache/cache/epub/'
CATALOG_URL = "https://www.gutenberg.org/cache/epub/feeds/rdf-files.tar.bz2"
CATALOG_ZIP = 'gutemberg/data.cache/rdf-files.tar.bz2'
CACHE_ROOT = 'gutemberg/data.cache/'

def parseCatalog(limit=None, init=1, store=False):
    print("Runing parser catalog", store)

    # Limit of iterations is the number of folders
    if limit is None:
        limit = sum(os.path.isdir(CACHE_PATH + i) for i in os.listdir(CACHE_PATH))

    print("With limits:", init, limit)

    # Check again that limits are correct
    initAndLimit(init, limit)

    parse(CACHE_PATH, limit=limit, init=init, store=store)

    print("Finished...........")

# Argument parser functions
def check_positive(value):
    ivalue = int(value)
    if ivalue <= 0:
        raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
    return ivalue

# Used to check that init is <= limit. If one is None return True
def initAndLimit(init, limit):
    if init is not None and limit is not None and init > limit:
            raise argparse.ArgumentTypeError("Error: --init should be smaller or equal than --limit")
    return True

def main(argv):
    print("Gutemberg.org catalog parser:")

    parser = argparse.ArgumentParser(description='Example:'
                                                 '\n\t* Parse with range of books:\n\t  run.py -p --init 22 --limit 32'
                                                 '\n\t* Download, extract, parse and store:\n\t  run.py -d -e -p -s',
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("-p", "--parse", help="Parse cached catalog on: '" + CACHE_PATH +
                        "'. Accept --init, --limit, --store|s", action="store_true")
    parser.add_argument("--init", help="Used with --parse|-p. Set init", type=check_positive)
    parser.add_argument("--limit", help="Used with --parse|-p. Set limit", type=check_positive)
    parser.add_argument("-s", "--store", help="NOT IMPLEMENTED. Used with --parse|-p. Store results on a database", action="store_true")
    parser.add_argument("-d", "--download", help="Download RDF catalog from " + CATALOG_URL + " to default " + CATALOG_ZIP,
                        type=str, const=CATALOG_ZIP, nargs='?')
    parser.add_argument("-e", "--extract", help="Extract " + CATALOG_ZIP + " to " + CACHE_ROOT + " or to specified dir",
                        type=str, const=CACHE_ROOT, nargs='?')

    args = parser.parse_args()

    if argv.__len__() == 0:
        parser.print_help(sys.stderr)
        sys.exit(0)
    if args.download:
        updater.download(CATALOG_URL, download_path=args.download)
    if args.extract:
        updater.extract(dest=args.extract)
    if args.parse:
        init = args.init or 1
        limit = args.limit
        initAndLimit(init, limit)
        parseCatalog(init=init, limit=limit, store=args.store)

if __name__ == '__main__':
    main(sys.argv[1:])

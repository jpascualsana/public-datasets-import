# ActivityPub importer

This script is able to show last activity of an ActivityPub user@domain. 

To get the activity of an ActivityPub user we need the address of this user, for example `somebody@someserver.com` or 
their outbox url, like: `https://someserver.com/users/somebody/outbox`.

We used [Gokberk Yaltirakli](https://www.gkbrk.com/2018/06/fetching-activitypub-feeds/) tutorial to get and parse the 
information. As said on the tutorial:

> Now; for the purposes of a blog post and for writing simple feed parsers, this code works with most servers. But this 
is not a fully spec-complient function for grabbing all the pages of content. 

And:

> Mastodon is not the only implementation of ActivityPub, and each implementation can do things in slightly different ways. While writing code to interact with ActivityPub servers, you should always consult the specification document.

So this mean that maybe not work on all the ActivityPub implementations. For the moment is tested on:

* **Mastodont**
* **PeerTube**

## Use

```
usage: activityPub.py [-h] [-u] [-f FEED [FEED ...]] [-a ACTIVITY] [--first]
                      [--updates UPDATES] [--verbose]
                      [address [address ...]]

Example:
	* Get 10 posts of ActivityPub address:
	  run.py user@server.dom -a 10 -v
	* Get first 10 activity for given addresses and feeds url:
	  run.py user@server.dom user@server2.dom -f https://server.dom/users/user/outbox -a 10
	* Get user info on json format 
	  run.py user@server.dom -u 
	* Get updates from specific post
	  run.py user@server.dom --updates https://server.dom/users/user/statuses/postId 

positional arguments:
  address               Ex: user@server.dom. One ore more ActivityPub address
                        where to retrieve the info

optional arguments:
  -h, --help            show this help message and exit
  -u, --user            Print user info in JSON format and exit. Only with
                        address, not for feed urls.
  -f FEED [FEED ...], --feed FEED [FEED ...]
                        Parse feed url.
  -a ACTIVITY, --activity ACTIVITY
                        Show last activity for given address or feed urls. By
                        default show all activity
  --first               Start parsing for first activity instead of last when
                        parsing posts
  --updates UPDATES     Get updates from specific post id
  --verbose, -v         Set logging to INFO with -v. Set logging to DEBUG with
                        -vv

```

If you want to retrieve updates from a specific post you can just:

```
run.py user@server.dom --updates https://server.dom/users/user/statuses/postId
```

## TODO's

* Functions to store on a database.

* Suport to files, images, and videos.
"""
Inspired on this tutorial:
https://gkbrk.com/2018/06/fetching-activitypub-feeds/
"""
import json
import logging, functools
from utils.request import Request
from utils.logger_utils import log

class ActivityPub(object):

    def __init__(self, address=None, request = None):
        self._request = request or Request()

        self.address = address
        self.wf_hdr = {'Accept': 'application/jrd+json'}
        self.as_header = {'Accept': 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"'}
        if address is not None:
            self.username, self.domain = address.split('@')
            # WebFinger data
            self.wf_url = 'https://{}/.well-known/webfinger'.format(self.domain)
            self.wf_par = {'resource': 'acct:{}'.format(address)}
            self._retrieve_actor()

    def _actor_is_set(self):
        if self.address is None:
            return False
        return True

    def _get_userUrl_or_url(self, url):
        if url is None:
            if not self._actor_is_set():
                raise Exception("No ActivityPub address is set or not feed url specified")
            url = self.actor.feed_url
        return url

    @log
    def _retrieve_actor(self):
        if not self._actor_is_set():
            raise Exception("No ActivityPub address is set, can't retrieve the actor")
        logging.info("Retrieving user info for " + self.address + " on " + self.wf_url)
        try:
            self.wf_resp = self._request.get(self.wf_url, timeout=None, headers=self.wf_hdr, fields=self.wf_par)
            # Get the activity pub json url
            matching = (link['href'] for link in self.wf_resp['links'] if link['type'] == 'application/activity+json')
            user_url = next(matching, None)
            user_json = self._request.get(user_url, timeout=None, headers=self.as_header)
            self.actor = Actor(self.address, user_url, user_json)
            return self.actor
        except:
            raise Exception("Can't retrieve user info")

    @log
    def _parse_feed(self, url=None, first=False):
        url = self._get_userUrl_or_url(url)
        logging.info("Parsing feed " + url)

        feed = self._request.get(url, headers=self.as_header)

        if 'orderedItems' in feed:
            for item in feed['orderedItems']:
                yield item

        # Get next url for pagination
        next_url = None
        # Go to the last post
        if not first:
            if 'last' in feed:
                next_url = feed['last']
            elif 'prev' in feed:
                next_url = feed['prev']
        # Go to the first post
        else:
            if 'first' in feed:
                next_url = feed['first']
            elif 'next' in feed:
                next_url = feed['next']

        if next_url:
            for item in self._parse_feed(next_url):
                yield item

    @log
    def get_feed_items(self, limit=None, url=None, first=False, limitId=None):
        logging.info("Getting feed items with limit " + str(limit))
        res = []
        i=0
        for item in self._parse_feed(url, first):
            logging.info("Parsing feed item num: " + str(i+1))
            try:
                # Only new tweets
                assert item['type'] == 'Create'
                content = item['object']['content']
                if item['object']['id'] == limitId:
                    break
                logging.info(content)
                res.append(item)
                i += 1
            except:
                continue
            if i == limit:
                break
        logging.info("Parsed " + str(res.__len__()))
        self.last_parsed_feeds = res
        return res

    '''
    Check if last known post is the last post published. Parameter last_known is the id of the last post
    '''
    @log
    def _is_updated(self, last_known, url=None):
        last = self.get_feed_items(limit=1, url=url)
        last_id = last[0]["object"]["id"]
        if last_id == last_known:
            return True
        return False

    def get_updates(self, last_known, url=None):
        url = self._get_userUrl_or_url(url)
        if not self._is_same_user(url, last_known):
            raise Exception("Provided last_known post url is not on the same user namespace")
        if self._is_updated(last_known, url):
            logging.info("Is already up to date!")
            return None
        return self.get_feed_items(url=url, limitId=last_known)


    '''
    Check if a post id is from the same user of an outbox
    '''
    def _is_same_user(self, outbox_id, post_id):
        if post_id.split("/statuses")[0] in outbox_id:
            return True
        return False

class Actor(object):
    def __init__(self, address, user_url, user_json):
        self.address = address
        self.user_url = user_url
        self.user_json = user_json
        self.feed_url = self.user_json['outbox']
    def __repr__(self):
        return json.dumps(self.user_json)
